"""
    This program shows how to run VideoProcessor with a detection model
    When you run this program, a new video will be displayed as a video-clip
    I recommend using one of my pretrained models (since their predict function is available)
"""
import cv2
import numpy as np
from prediction_utils import post_process, VideoProcessor, preprocess


if __name__ == "__main__":

    with VideoProcessor('PATH TO VIDEO',
                        preprocess, post_process) as v:
        for img in v:
            cv2.imshow('frame',  img.astype(np.uint8))
            if cv2.waitKey(1) & 0xFF == ord('q'):
                break
        cv2.destroyAllWindows()
