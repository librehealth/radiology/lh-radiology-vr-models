"""
    This program shows how to run VideoProcessor with a segmentation model
    When you run this program, a new video will be displayed as a clip
    I recommend using one of my pretrained models (since their predict function is available)
"""
import cv2, numpy as np
from prediction_utils import segmentation_inference, preprocess, VideoProcessor, CMAP
SEGMENT_SINGLE = True
CHOSEN_CLASS = 10 # this class denotes surgical tools


if __name__ == "__main__":
    """This example allows segmenting the whole image"""
    if not SEGMENT_SINGLE:
        with VideoProcessor('PATH TO VIDEO',
                            False,
                            preprocess, segmentation_inference.predict) as v:
            for i in v:
                img = i.astype(np.uint8)
                img = CMAP[img, :]
                cv2.imshow('frame',  img.astype(np.uint8))
                if cv2.waitKey(1) & 0xFF == ord('q'):
                    break
            cv2.destroyAllWindows()
    else:
        """However, in some cases only one class should be segmented on video"""
        with VideoProcessor('PATH TO VIDEO',
                            True,
                            preprocess, segmentation_inference.predict) as v:
            for i, oroginal in v:
                img = i.astype(np.uint8)
                oroginal = cv2.cvtColor(oroginal, cv2.COLOR_BGR2RGB)
                oroginal = preprocess(oroginal).astype(np.uint8)
                oroginal = cv2.resize(oroginal, (256, 256))
                color = np.array([255, 0, 0], dtype='uint8')
                mask = img==CHOSEN_CLASS
                masked_img = np.where(mask[..., None], color, oroginal)
                out = cv2.addWeighted(oroginal, 0.5, masked_img, 0.5,0.2)
                cv2.imshow('frame', out.astype(np.uint8))
                if cv2.waitKey(1) & 0xFF == ord('q'):
                    break
            cv2.destroyAllWindows()
