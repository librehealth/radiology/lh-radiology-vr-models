import argparse
import json
import os

from pathlib import Path

from inference.detection.model import model


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='run segmentation model from console.')
    parser.add_argument("--img-dir",  type=str, required=True,
                        help="path to the directory with images")
    parser.add_argument("--save-to", type=str, required=True,
                        help="masks will be saved here")
    args = parser.parse_args()

    img_dir = Path(args.img_dir)
    save_to = Path(args.save_to)
    if not os.path.exists(save_to):
        os.mkdir(save_to)

    assert os.path.isdir(save_to), "invalid directory name"

    predictions = []
    for file in img_dir.iterdir():
        prediction = model.predict(file.__str__())
        prediction_path = save_to / file.name
        prediction.save(output_path=prediction_path.__str__())
        predictions.append(prediction.json())
        print(predictions)

    with open(save_to / 'predictions.json', 'w') as file:
        json.dump(predictions, file)