from roboflow import Roboflow
from inference.detection.config import *


def get_model():
    rf = Roboflow(api_key=PRIVATE_RF_API)
    project = rf.workspace().project(RF_PROJECT_ID)
    version = project.version(1)
    model = version.model
    return model


model = get_model()