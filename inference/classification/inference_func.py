from model import predict
import cv2
import numpy as np


def predict_arr(input_array: np.ndarray,
                probability_only: bool = True):
    preds = predict(input_array)
    return preds if probability_only else np.argmax(preds, axis=1)


def predict_file(filename: str,
                 probability_only: bool = True):
    image = cv2.cvtColor(cv2.imread(filename), cv2.COLOR_BGR2RGB)
    image = image[np.newaxis, :, :, :]
    preds = predict(image)
    return preds if probability_only else np.argmax(preds, axis=1)